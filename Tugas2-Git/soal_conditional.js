const prompt = require('prompt-sync')();

const name = prompt('Masukkan nama anda : ');

if (name == '') {
    console.log('Nama tidak boleh kosong');
    return ;
}

const role = prompt(`Halo ${name} Masukkan role (Werewolf, Penyihir, Guard) : `);


if (role.toLowerCase() != 'werewolf' && role.toLowerCase() != 'penyihir' && role.toLowerCase() != 'guard') {
    console.log('Role tidak sesuai');
    return ;
}

if (role.toLowerCase() == 'penyihir'){
    console.log(`Selamat datang di Dunia Werewolf, ${name}! \nHalo ${role} ${name}, kamu dapat melihat siapa yang menjadi werewolf!`)
}

if (role.toLowerCase() == 'guard'){
    console.log(`Selamat datang di Dunia Werewolf, ${name}! \nHalo ${role} ${name}, kamu akan membantu melindungi temanmu dari serangan werewolf!`)
}

if (role.toLowerCase() == 'werewolf'){
    console.log(`Selamat datang di Dunia Werewolf, ${name}! \nHalo ${role} ${name}, Kamu akan memakan mangsa setiap malam!`)
}
